import MicroModal from 'micromodal';  // es6 module
import {Swiper, Mousewheel, Pagination, Navigation, EffectCoverflow } from 'swiper'
import axios from 'axios';

Swiper.use([ Mousewheel, Pagination, Navigation, EffectCoverflow ])
document.addEventListener('DOMContentLoaded', () => {
	console.log('текущий относительный url: '+document.location.pathname);

	if (document.location.pathname == '/') {

		function initSwiperNumericPagination(){
			let slide_total = document.querySelectorAll(".tovar .gallery__slide").length;
			document.querySelector(".tovar .swiper-counter > .total").innerHTML = slide_total;
		}
		initSwiperNumericPagination()

		function updSwiperNumericPagination() {
			this.el.querySelector(".tovar .swiper-counter > .count").innerHTML = this.realIndex + 1;
		}


		const gallery = new Swiper('.tovar > .gallery', {

			// Optional parameters
			direction: 'horizontal',
			loop: true,
			effect: "coverflow",
			speed: 1000,
			slidesPerView: 1,
			grabCursor: true,  // Если установлено значение true, указатель изменится на форму ладони, когда мышь накрывает Swiper, а указатель изменится на форму ручки при перетаскивании. (Зависит от формы браузера)
			noSwiping: true,
			setWrapperSize: true, // Этот параметр добавит ширину или высоту, равную сумме слайдов в Wrapper, что может понадобиться в браузерах, которые не очень хорошо поддерживают макет flexbox.
			noSwipingClass: "no-swipe",
			parallax: false,
			touchReleaseOnEdges: true,


			// mousewheel: {
			// 	invert: false, //скроллинг слайдера колёсиком мыши
			// },

			pagination: {
				el: '.slider-pagination',
				clickable: true,
			},

			// Navigation arrows
			navigation: {
				nextEl: '.swiper-button-next',
				prevEl: '.swiper-button-prev',
			},

			on: {
				//init: updSwiperNumericPagination,
				slideChange: updSwiperNumericPagination
			}

		})



		function initSwiperNumericPagination2(){
			let slide_total = document.querySelectorAll(".modal-gallery .gallery__slide").length;
			document.querySelector(".modal-gallery .swiper-counter > .total").innerHTML = slide_total;
		}
		initSwiperNumericPagination2()

		function updSwiperNumericPagination2() {
			this.el.querySelector(".modal-gallery .swiper-counter > .count").innerHTML = this.realIndex + 1;
		}


		const gallery_modal = new Swiper('.modal-gallery', {

			// Optional parameters
			direction: 'horizontal',
			loop: true,
			effect: "coverflow",
			speed: 1000,
			slidesPerView: 1,
			grabCursor: true,  // Если установлено значение true, указатель изменится на форму ладони, когда мышь накрывает Swiper, а указатель изменится на форму ручки при перетаскивании. (Зависит от формы браузера)
			noSwiping: true,
			setWrapperSize: true, // Этот параметр добавит ширину или высоту, равную сумме слайдов в Wrapper, что может понадобиться в браузерах, которые не очень хорошо поддерживают макет flexbox.
			noSwipingClass: "no-swipe",
			parallax: false,
			touchReleaseOnEdges: true,


			// mousewheel: {
			// 	invert: false, //скроллинг слайдера колёсиком мыши
			// },

			pagination: {
				el: '.slider-pagination',
				clickable: true,
			},

			// Navigation arrows
			navigation: {
				nextEl: '.swiper-button-next',
				prevEl: '.swiper-button-prev',
			},

			on: {
				//init: updSwiperNumericPagination,
				slideChange: updSwiperNumericPagination2
			}

		})


		const swipeAllSliders = (index) => {
			gallery_modal.slideTo(index);
			gallery.slideTo(index);
		}

		gallery_modal.on('slideChange', () => swipeAllSliders(gallery_modal.activeIndex));
		gallery.on('slideChange', () => swipeAllSliders(gallery.activeIndex));

		

		//MicroModal.init();
		MicroModal.init({
			onShow: modal => console.info(`${modal.id} is shown`), // [1]
			onClose: modal => console.info(`${modal.id} is hidden`), // [2]
			openTrigger: 'data-custom-open', // [3]
			closeTrigger: 'data-custom-close', // [4]
			disableScroll: true, // [5]
			disableFocus: false, // [6]
			awaitCloseAnimation: false, // [7]
			debugMode: true // [8]
		});
		
		let button = document.querySelector('.js_show_modal');
		button.addEventListener('click', function(){
			MicroModal.show('modal-1');    
		});

		let buttons2 = document.querySelectorAll('.gallery__slide__bg');
		buttons2.forEach(function(elem) {
			elem.addEventListener('click', function(){
				MicroModal.show('modal-2');
			});
		});

		// let prev_btn_modal = document.querySelector('.js_prev_btn_modal');
		// let next_btn_modal = document.querySelector('.js_next_btn_modal');
		// let prev_btn_main = document.querySelector('.js_prev_btn_main');
		// let next_btn_main = document.querySelector('.js_next_btn_main');
		// prev_btn_modal.addEventListener('click', function(){
		// 	prev_btn_main.click();    
		// });
		// prev_btn_main.addEventListener('click', function(){
		// 	prev_btn_modal.click();    
		// });
		// next_btn_modal.addEventListener('click', function(){
		// 	next_btn_main.click();    
		// });
		// next_btn_main.addEventListener('click', function(){
		// 	next_btn_modal.click();    
		// });


		// Валидация формы "Уведомить о поступлении"
		// Проверяет введён ли хотя бы один контакт notify-form__send-btn
		let notify_form_btn_send = document.querySelector('#notify-form__send-btn');
		let success = document.querySelector(".success");
		notify_form_btn_send.addEventListener('click', function(){
			let phone = document.getElementById('notify-form__phone');
			let email = document.getElementById('notify-form__email');
			phone.addEventListener("input", validation, false);
			email.addEventListener("input", validation, false);
			function validation() {
				if ((!phone.value) && (!email.value)) {
					phone.setCustomValidity ('Введите хотя бы один контакт');			
				}
				else {
					phone.setCustomValidity ('');
					//MicroModal.close('modal-1');
					success.style.display = 'block';
				}
			}
			validation() 
		});


		/*Отправка формы "Уведомить о поступлении" в телеграм*/
		const TG_BOT_TOKEN = '5948102317:AAHdb4jpUkKLDi2TMp8KaHqoy88gIiaPWi8'
		const TG_CHAT_ID = '-1001828498994'
		const URL_API = `https://api.telegram.org/bot${ TG_BOT_TOKEN }/sendMessage`
		document.getElementById('notify-form').addEventListener('submit', function(e) {
			e.preventDefault();
			let message = `<b>Сообщить о поступлении:</b>\n`;
			message += `<b>Телефон: </b>${this.notify_form_phone.value} \n`;
			message += `<b>Почта: </b>${this.notify_form_email.value} \n`;
			axios.post(URL_API, {
				chat_id: TG_CHAT_ID,
				parse_mode: 'html',
				text: message
			})

		}) 

	}
	
	/*Отправка формы "ОПЛАТИЛ(А)" в телеграм*/
	if (document.location.pathname == '/oformit_zakaz.html') {
		//console.log('клац 0');
		let btn_load_check = document.querySelector('#order_form_check_btn');
		btn_load_check.addEventListener('click', function(){
			let file_check_input = document.querySelector('#order_form_check');
			//console.log('клац');
			file_check_input.click();
		})
		let load_button = document.getElementById("order_form_check_btn");
		let file_input = document.getElementById("order_form_check");
		file_input.addEventListener('change', function(e) {
			load_button.style.borderColor = 'green';
		})

		const TG_BOT_TOKEN = '5948102317:AAHdb4jpUkKLDi2TMp8KaHqoy88gIiaPWi8'
		const TG_CHAT_ID = '-1001828498994'
		const URL_API = `https://api.telegram.org/bot${ TG_BOT_TOKEN }/sendMessage`
		document.getElementById('order_form').addEventListener('submit', function(e) {
			e.preventDefault();
			let message = `<b>Новый заказ!</b>\n`;
			message += `<b>Имя: </b>${this.order_form_name.value} \n`;
			message += `<b>Фамилия: </b>${this.order_form_surname.value} \n`;
			message += `<b>Телефон: </b>${this.order_form_phone.value} \n`;
			message += `<b>Город: </b>${this.order_form_city.value} \n`;
			message += `<b>Улица: </b>${this.order_form_street.value} \n`;
			message += `<b>Номер дома: </b>${this.order_form_house_number.value} \n`;
			message += `<b>Комментарий к заказу: </b>${this.order_form_comment.value} \n`;
			axios.post(URL_API, {
				chat_id: TG_CHAT_ID,
				parse_mode: 'html',
				text: message
			})

			// let FR = new FileReader();
			// let myImage = document.querySelector("#image");
			// FR.onload = function (event){
			// 	let contents = event.target.result;
			// 	myImage.src = contents;
			// }
			// let file = FR.readAsDataURL(document.querySelector("#order_form_check"));
			let file = document.getElementById("order_form_check");
			let success = document.querySelector(".success");

			const formData = new FormData();
			formData.append('chat_id', TG_CHAT_ID)
			formData.append('document', file.files[0])
			formData.append('caption', 'Чек об оплате')

			axios.post(`https://api.telegram.org/bot${ TG_BOT_TOKEN }/sendDocument`,  formData, {
				headers: {
					'Content-Type': 'multipart/form-data'
				}
			})
			.then((res) => {
				// this.order_form_name.value = '';
				// this.order_form_surname.value = '';
				// this.order_form_phone.value = '';
				// this.order_form_city.value = '';
				// this.order_form_street.value = '';
				// this.order_form_house_number.value = '';
				// this.order_form_comment.value = '';
				success.style.display = 'block';
			})

		})

	}


})
